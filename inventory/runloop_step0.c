/* void move_xy(_entity_struct_ entity, index from, vector to) {
	to_index = from;
	if (to.y) to_index += to.y*entity.x_len;
	if (to.x) to_index += to.x;
	entity.len[to_index] = entity.len[from];
	 // set entity.len[from] to a blank space ...??
} */

int xy_to_index(x_limit,x,y) {
	int index = 0;
	if (y) index += y*x_limit;
	if (x) index += x;
	return index;
}

char id_to_char(_entity_struct_ entity) {
	return char_theme[entity.id];
}

void render_inventory(_entity_struct_ entity, char *char_buffer) {
	for (i=0;i<entity.len;i++)
		char_buffer[i] = id_to_char(entity.inv[i]);
}

