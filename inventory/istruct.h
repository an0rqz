/*
    This file is part of an0rqz. Copyright 2009 ze, Quazgaa.

    an0rqz is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    an0rqz is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with an0rqz.  If not, see <http://www.gnu.org/licenses/>.
*/

typedef struct {
	int length;
	int dimensions;
	int *inventory;
} inventory;

