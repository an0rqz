randomly generated grid world including outer boundaries, walls, rooms,
  hallways, doors, stairs up, stairs down, all spaces connected (should follow
  reliably from appropriate path generation algorithm), monsters, treasures
players with inventory, some sorts of stats and abilities, equipment, movement,
  attacks, defenses, ways to gain and lose strength
monsters with rudimentary AI's, movement, sometimes equipment, sometimes
  inventory/treasures to drop,   various stats, attacks, defenses

monsters are ranked, rank matching is considered in world generation dependant
  upon dungeon level?
multiple players possible, level is genrated upon entry, persistent until last
  place-holder is removed... place-holders include players, maybe special items?

